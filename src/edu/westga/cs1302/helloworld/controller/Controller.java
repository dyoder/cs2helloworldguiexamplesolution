package edu.westga.cs1302.helloworld.controller;

import java.util.ArrayList;
import java.util.List;

import javafx.fxml.FXML;
import javafx.scene.control.CheckBox;
import javafx.scene.control.Label;
import javafx.scene.control.TextArea;
import javafx.scene.control.TextField;

/**
 * The Controller Class.
 * 
 * @author CS 1302
 */
public class Controller {

	@FXML
	private TextField nameTextField;

	@FXML
	private TextArea namesTextArea;

	@FXML
	private Label greetingLabel;

	@FXML
	private CheckBox spanishCheckBox;

	private List<String> names;
	private String currentName;

	/**
	 * Instantiates a new Controller
	 */
	public Controller() {
		this.names = new ArrayList<String>();
		this.currentName = "";
	}

	@FXML
	private void initialize() {
		this.greetingLabel.setText("Hello, World!");
		this.nameTextField.setPromptText("Enter a name");
		this.namesTextArea.setText("");
		this.namesTextArea.setEditable(false);
	}

	@FXML
	private void handleSubmit() {
		this.currentName = this.nameTextField.getText();
		this.setGreeting();
		
		if (this.currentName != null && !this.currentName.isEmpty()) {
			this.nameTextField.setText("");
			this.nameTextField.setPromptText("Enter a name");
			this.names.add(this.currentName);
			this.buildAndSetNamesOutput();
		}
	}

	private void buildAndSetNamesOutput() {
		String namesOutput = "";
		
		for (String currName : this.names) {
			namesOutput += currName + System.lineSeparator();
		}
		
		this.namesTextArea.setText(namesOutput);
	}

	@FXML
	private void handleLanguageSelection() {
		this.setGreeting();
	}

	private void setGreeting() {
		String greeting = "Hello, ";
		
		if (this.spanishCheckBox.isSelected()) {
			greeting = "Hola, ";
		}
		
		if (this.currentName != null && !this.currentName.isEmpty()) {
			greeting += this.currentName;
		} else {
			greeting += "World";
		}
		
		greeting += "!";
		this.greetingLabel.setText(greeting);
	}
}
